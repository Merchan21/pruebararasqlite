package com.facci.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class actividad extends AppCompatActivity {

    TextView datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad);

        datos = findViewById(R.id.Datos);

        String Tempholder = getIntent().getStringExtra("Lista");
        datos.setText(Tempholder);
    }
}
