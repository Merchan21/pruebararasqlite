package com.facci.prueba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView listView;

    String []listviewitems = new String[]{

            "Miryam Elizabeth"+"\n"+"Felix Lopez"+"\n"+"Dr. C",
            "Medardo "+"\n"+"Mora Solorzano"+"\n"+"Dr. C",
            "Ximena "+"\n"+"Guillen Vivas"+"\n"+"DDS. MSc. PhD",
            "Vicente"+"\n"+"Véliz Briones"+"\n"+"Ing.",
            "Lino Mauro"+"\n"+"Toscanini Segale"+"\n"+"Econ. PhD",
            "Carlos "+"\n"+"Mutafar"+"\n"+"Dr. C"

    };

    String []listviewitemss = new String[]{
            "ESPAM"+"\n"+"Escuela Superior Politecnica Agropecuaria"+"\n"+"Ingenieria en sistemas, odontologia"+"\n"+"B",
            "ULEAM"+"\n"+"Universidad Laica Eloy alfaro de Manabi"+"\n"+"Medicina, Agronomia"+"\n"+"B",
            "USGP"+"\n"+"Universidad San Gregorio de Portoviejo"+"\n"+"Mecanica Automotris, aeronautica"+"\n"+"B",
            "UTM"+"\n"+"Universidad Tecnica de Manabi"+"\n"+"Ingenieria en sistemas, odontologia"+"\n"+"C",
            "UCSG"+"\n"+"Universidad Catolica de Santiago de Guayaquil"+"\n"+"Ingenieria Civil "+"\n"+"B",
            "USFQ"+"\n"+"Universidad San Francisco de Quito"+"\n"+"Electica"+"\n"+"A",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.Lista);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_2, android.R.id.text1, listviewitems);
        ArrayAdapter<String>adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_2, android.R.id.text1, listviewitemss);
        listView.setAdapter(adapter);
        listView.setAdapter(adapter1);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String Templistview = listviewitems[position].toString();
                Intent intent = new Intent(MainActivity.this, actividad.class);
                intent.putExtra("Listviewclickvalue", Templistview);
                intent.putExtra("Lista", Templistview);
                startActivity(intent);
            }
        });

    }
}
